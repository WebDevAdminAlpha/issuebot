#!/usr/bin/env python3
#
#
# issuebot_pip_install = androguard

import inspect
import json
import os
import sys

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..'))
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
import issuebot
from issuebot import IssuebotModule


class FDroidScanner(IssuebotModule):

    link_url = None

    def gen_report_line(self, line):
        msg, f = line
        return '<li>%s: <a href="%s">%s</a></li>' % (msg, self.get_source_url(f), f)

    def main(self):
        cmd = ['fdroid', 'scanner', '--force', '--json', self.application_id]
        p = issuebot.run_cli_tool(cmd)
        if p.returncode != 0:
            print(p.stdout.decode())
            return
        data = json.loads(p.stdout.decode())
        report = ''
        if self.application_id in data:
            report += '<h3>fdroid scanner</h3>'
            for versionCode in sorted(data[self.application_id].keys(), key=int, reverse=True):
                v = data[self.application_id][versionCode]
                errors = v.get('errors', [])
                warnings = v.get('warnings', [])
                infos = v.get('infos', [])
                if errors or warnings or infos:
                    report += '<details'
                    if errors or warnings:
                        report += ' open="true"'
                    report += '><summary><em>versionCode</em>: <big><tt>%s</tt></big></summary>' % versionCode
                if errors:
                    self.add_label('scanner-error')
                    report += '<details open="true"><summary><b>Errors</b></summary><ul>'
                    for error in errors:
                        report += self.gen_report_line(error)
                    report += '</ul></details>'
                if warnings:
                    self.add_label('scanner-warning')
                    report += '<details><summary>Warnings</summary><ul>'
                    for warning in warnings:
                        report += self.gen_report_line(warning)
                    report += '</ul></details>'
                if infos:
                    self.add_label('scanner-info')
                    report += '<details><summary>Infos</summary><ul>'
                    for info in infos:
                        report += self.gen_report_line(info)
                    report += '</ul></details>'
                if errors or warnings or infos:
                    report += '</details>'

        if report:
            self.reply['report'] = report
        if data:
            self.reply['reportData'] = data
        self.write_json()


if __name__ == '__main__':
    FDroidScanner().main()
