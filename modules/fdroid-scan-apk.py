#!/usr/bin/env python3

import inspect
import os
import sys

from fdroidserver import update

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..'))
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule


class FDroidScanApk(IssuebotModule):

    def main(self):
        if not self.apkfiles_required():
            return
        report = """<h3><tt>uses-permission</tt> and <tt>uses-permission-sdk-23</tt> from APK</h3>"""
        for apkfile in self.apkfiles:
            apk = update.scan_apk(apkfile)
            self.reply['reportData'][os.path.basename(apkfile)] = apk
            report += '<details><summary>{}</summary><ul>'.format(os.path.basename(apkfile))
            uses_permissions = apk.get('uses-permission', [])
            if uses_permissions:
                report += '<code>uses-permission</code><ul>'
                for p in uses_permissions:
                    name = p[0]
                    if name.startswith('android.permission.'):
                        report += '<li><a href="https://developer.android.com/reference/android/Manifest.permission#'
                        report += name.split('.')[2]
                        report += '"><tt>{}</tt></a></li>'.format(name)
                    else:
                        report += '<li><tt>{}</tt></li>'.format(name)
                report += '</ul>'
            report += '</ul></details>'

        self.reply['report'] = report
        self.write_json()


if __name__ == '__main__':
    FDroidScanApk().main()
